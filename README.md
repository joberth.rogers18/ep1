## Projeto EP1
### Conway Game of life

   
   Este "jogo" é na realidade um jogo sem jogador, o que quer dizer que sua evolução
é determinada pelo seu estado inicial, não necessitando de nenhuma entrada de 
jogadores humanos. Ele é jogado em um conjunto de células quadradas que seguem 
ao infinito em todas as direções. Cada célula tem oito "vizinhos", que são as 
células adjacentes, incluindo as diagonais. Cada célula pode estar em dois 
estados: "viva" ou "morta". (Também são usados os termos "ligado" e "desligado".)
O estado do tabuleiro evolui e se modifica em pequenas passagens de tempo. Os 
estados de todas as células em um instante são considerados para calcular o estado
de todas as células no instante seguinte. Todas as células são atualizadas
simultaneamente. As transições dependem apenas do número de vizinhos vivos
(ver as regras acima).
    A ideia básica do "jogo" é começar com uma configuração simples de células vivas
(organismos) que são colocadas em um tabuleiro 2D de vários métodos. Isto
constitui a primeira geração. As "leis genéticas" de Conway para nascimentos, 
mortes e sobrevivência (as quatro regras acima) são então aplicadas e a nova 
geração é então colocada de acordo. Geração a geração os "jogador(es)" observam
as várias imagens que surgem.

## Regra do Jogo

As regras são simples e elegantes:

Qualquer célula viva com menos de dois vizinhos vivos morre de solidão.
Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação.
Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva.
Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração.

## COMANDOS(IMPORTANTE)

Para poder roda o programa, basta ir na pasta onde o programa se encontra 
e usar o seguintes comando, se o arquivo for modificado pela pessoa que esta
com o codigo em si, basta usar o comando **make**, e para poder rodar o codigo
o comando é **make run**, caso os headers for modificados, use o comando **make clean**
e use os comandos anteriores em ordem.

mas de começo >> use **make** e logo após **make run** e aperte enter, para executar
o programa.

Logo após haverá um menu interativo onde o usuario vai poder escolher entre, ver a
como se comporta um glider apertando a opção "1", sendo ele padrao ou em qualquer
canto da matriz que o usuario escolher, ou então ver a gosper glider gun, apertando
o numero "2", e por ultimo a exploder apertando "3", outra forma que o usuario pode
interagir colocando a posição ou apenas vendo essa forma funcionar
